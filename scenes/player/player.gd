extends "res://engine/entity.gd"


func _ready():
	SPEED = 70

func _physics_process(delta):
	controls_loop()
	movement_loop()
	spritedir_loop()
	
	if is_on_wall():
		if spritedir == "left" and test_move(transform, Vector2.LEFT):
			anim_switch("idle")
		if spritedir == "right" and test_move(transform, Vector2.RIGHT):
			anim_switch("idle")
		if spritedir == "up" and test_move(transform, Vector2.UP):
			anim_switch("idle")
		if spritedir == "down" and test_move(transform, Vector2.DOWN):
			anim_switch("idle")
	if movedir != Vector2.ZERO:
		anim_switch("walk")
	else:
		anim_switch("idle")


func controls_loop():
	var LEFT = Input.is_action_pressed("player_left")
	var RIGHT = Input.is_action_pressed("player_right")
	var UP = Input.is_action_pressed("player_up")
	var DOWN = Input.is_action_pressed("player_down")
	
	movedir.x = - int(LEFT) + int(RIGHT)
	movedir.y = - int(UP) + int(DOWN)
	





