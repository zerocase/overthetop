extends KinematicBody2D


var SPEED = 0
var movedir = Vector2.ZERO
var spritedir = "left"

func movement_loop():
	var motion = movedir.normalized() * SPEED
	move_and_slide(motion, Vector2.ZERO)
	
func spritedir_loop():
	match movedir:
		Vector2.LEFT:
			spritedir = "left"
		Vector2(-1,1):
			spritedir = "left"
		Vector2(-1,-1):
			spritedir = "left"
		Vector2.RIGHT:
			spritedir = "right"
		Vector2(1,1):
			spritedir = "right"
		Vector2(1,-1):
			spritedir = "right"

func anim_switch(animation):
	var newanim = str(animation,spritedir)
	if $anim.current_animation != newanim:
		$anim.play(newanim)

